package de.egore911.appframework.maven;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;
import org.jtwig.JtwigModel;
import org.jtwig.JtwigTemplate;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Mojo(name = "generate", threadSafe = true)
public class GenerateStubs extends AbstractMojo {

    private static class Generation {
        final String directory;
        final boolean addDir;
        final String[] templates;

        Generation(String directory, boolean addDir, String[] templates) {
            this.directory = directory;
            this.addDir = addDir;
            this.templates = templates;
        }
    }

    private static final Generation TEMPLATES[] = {
            new Generation("src/main/java", true, new String[]{
                    "persistence/model/{{typeCase}}Entity.java",
                    "persistence/selector/{{typeCase}}Selector.java",
                    "ui/dto/{{typeCase}}.java",
                    "ui/rest/{{typeCase}}Service.java",
                    "util/mappers/{{typeCase}}Mapper.java"
            }),
            new Generation("src/main/resources/META-INF/resources", false, new String[]{
                    "app/module/{{typeLower}}/{{typeLower}}.controller.js",
                    "app/module/{{typeLower}}/{{typeLower}}.d.ts",
                    "app/module/{{typeLower}}/{{typeLower}}.factory.js",
                    "app/module/{{typeLower}}/{{typeLower}}.html",
                    "app/module/{{typeLower}}/{{typeLower}}s.controller.js",
                    "app/module/{{typeLower}}/{{typeLower}}s.html"
            }),
            new Generation("src/test/java", true, new String[]{
                    "persistence/dao/{{typeCase}}DaoTest.java",
                    "persistence/selector/{{typeCase}}SelectorTest.java",
                    "ui/{{typeCase}}CRUDTest.java"
            }),
            new Generation("target/generated-sources/annotations", true, new String[]{
                    "persistence/model/{{typeCase}}Entity_.java"
            })
    };

    @Parameter(defaultValue = "${project}", readonly = true, required = true)
    private MavenProject project;

    @Parameter(defaultValue = "${type}", required = true)
    private String type;

    @Parameter(defaultValue = "${dir}")
    private String dir;

    @Parameter(defaultValue = "${force}")
    private boolean force;

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {

        String typeLower = type.toLowerCase();
        String typeUpper = type.toUpperCase();
        String typeCase = type.substring(0, 1).toUpperCase() + type.substring(1);

        if (dir == null || dir.isEmpty()) {
            dir = project.getGroupId().replace('.', '/');
        } else {
            dir = dir.replace('\\', '/');
        }

        JtwigModel model = JtwigModel.newModel()
                .with("typeLower", typeLower)
                .with("typeUpper", typeUpper)
                .with("typeCase", typeCase)
                .with("package", dir.replace('/', '.'))
                .with("project", project.getGroupId().substring(project.getGroupId().lastIndexOf('.') + 1));

        String sourceDirectory = project.getBasedir().getAbsolutePath();

        for (Generation g : TEMPLATES) {
            for (String template : g.templates) {
                JtwigTemplate t1 = JtwigTemplate.inlineTemplate(template);

                JtwigTemplate t = JtwigTemplate
                        .classpathTemplate("de/egore911/appframework/maven/templates/" + g.directory + '/' + template + ".twig");

                Path path = Paths.get(sourceDirectory + '/' + g.directory + '/' + (g.addDir ? dir : "") + '/' + t1.render(model));

                try {
                    if (!Files.exists(path.getParent())) {
                        Files.createDirectories(path.getParent());
                    }
                    if (Files.exists(path)) {
                        if (!force) {
                            getLog().warn("Will not overwrite " + path);
                            continue;
                        }
                    } else {
                        Files.createFile(path);
                    }
                    try (FileOutputStream fos = new FileOutputStream(path.toFile())) {
                        t.render(model, fos);
                    }
                } catch (IOException e) {
                    throw new MojoExecutionException(e.getMessage(), e);
                }
            }
        }
    }

}
