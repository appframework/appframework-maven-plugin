# Purpose

appframework supports several standard use cases to build master detail applications.
This plugin supports building these views by generating the necessary stubs. Ideally
it will generate 90% of the necessary plumbing / groundwork to get you up and running
with a new type

# Generation

This plugin generates the following building blocks:

- the entity
- the selector for the entity
- the DAO for the entity
- the DTO for the REST interface
- the master and detail views using angular 1 and bootstrap
- the tests for the DAO and the selector

# Usage

To generate the type "MyType" simply call:

    mvn de.egore911.appframework:appframework-maven-plugin:generate -Dtype=MyType

Existing files will not be overwritten. If you want to
overwrite existing file, pass the -Dforce=true parameter

    mvn de.egore911.appframework:appframework-maven-plugin:generate -Dtype=MyType -Dforce=true
