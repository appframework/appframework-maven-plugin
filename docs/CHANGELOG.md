## [2.1.1](https://gitlab.com/appframework/appframework-maven-plugin/compare/2.1.0...2.1.1) (2025-02-26)

# [2.1.0](https://gitlab.com/appframework/appframework-maven-plugin/compare/2.0.1...2.1.0) (2024-10-28)


### Bug Fixes

* **deps:** update dependency org.apache.maven:maven-plugin-api to v3.9.7 ([5b7b12d](https://gitlab.com/appframework/appframework-maven-plugin/commit/5b7b12defac72c733b08c5e4c9bfc9bf15d982c6))
* **deps:** update dependency org.apache.maven:maven-plugin-api to v3.9.8 ([04e385f](https://gitlab.com/appframework/appframework-maven-plugin/commit/04e385f6c56ecb41e40df4764b6dc2ae66905a1e))
* **deps:** update dependency org.apache.maven:maven-plugin-api to v3.9.9 ([a649945](https://gitlab.com/appframework/appframework-maven-plugin/commit/a649945d36f57916a168d43db4bf7210e13e7398))
* **deps:** update dependency org.apache.maven.plugin-tools:maven-plugin-annotations to v3.12.0 ([e197b99](https://gitlab.com/appframework/appframework-maven-plugin/commit/e197b998eb2c4e1cfa6d404b60093ae09e096623))
* **deps:** update dependency org.apache.maven.plugin-tools:maven-plugin-annotations to v3.13.0 ([c35a56b](https://gitlab.com/appframework/appframework-maven-plugin/commit/c35a56b77b917f6856f727cccf5972c3d479dfb2))
* **deps:** update dependency org.apache.maven.plugin-tools:maven-plugin-annotations to v3.13.1 ([d5af983](https://gitlab.com/appframework/appframework-maven-plugin/commit/d5af983d2220e86cb8f682f318a4cbd4e7a8b546))
* **deps:** update dependency org.apache.maven.plugin-tools:maven-plugin-annotations to v3.15.0 ([33f8274](https://gitlab.com/appframework/appframework-maven-plugin/commit/33f8274ca5a7c14bc3844e7693e6ad9241c42132))
* **deps:** update dependency org.apache.maven.plugin-tools:maven-plugin-annotations to v3.15.1 ([4adae24](https://gitlab.com/appframework/appframework-maven-plugin/commit/4adae249bf7ce340684cdc6b4ee7b7f6da17d1e2))


### Features

* **ci:** Add .gitlab-ci.yml ([0e1fed6](https://gitlab.com/appframework/appframework-maven-plugin/commit/0e1fed6c79e0e9d564de2f65cd596667dee0e599))
